import { RowChart } from './RowChart';
import { ModelForm } from '../jogo/ModelForm';

interface IGraficos extends ng.IScope {
    erroBackend: boolean;
    listDados: ModelForm[];
    faixaEtariaChartObject: any;
    faixaEtariaLoading: boolean;

    aprendizagemChartObject: any;
    aprendizagemloading: boolean;

    calculoIPontuacaoChartObject: any;
    calculoIPontuacaoLoading: boolean;

    nivelEnsinoChartObject: any;
    nivelEnsinoLoading: boolean;

    versaoVRChartObject: any;
    versaoVRloading: boolean;

    calculoIChartObject: any;
    calculoILoading: boolean;
}

export class GraficosController {

  public static $inject: string[] = new Array<string>('$scope', 'toastr', '$http');
  public static ID: string = 'GraficosController';
  public $scope: IGraficos;
  private toastr: any;
  private $http: ng.IHttpService;
  private googlechart: any;
  private URL_BACKEND = 'http://realidadeintegral.tambacode.com.br/api/public/list-dados';
  // private URL_BACKEND = 'http://localhost:8000/list-dados';

  /* @ngInject */
  constructor ($scope: IGraficos, toastr: any, $http: ng.IHttpService, googlechart: any) {
    this.$scope = $scope;
    this.toastr = toastr;
    this.$http = $http;
    this.googlechart = googlechart;

    this.init();
  }

  public init(): void {
    this.$scope.erroBackend = false;
    this.$scope.faixaEtariaLoading = true;
    this.$scope.aprendizagemloading = true;
    this.$scope.calculoIPontuacaoLoading = true;
    this.$scope.nivelEnsinoLoading = true;
    this.$scope.versaoVRloading = true;
    this.$scope.calculoILoading = true;

    this.listDados();
  }

  public geraGraficoFaixaEtaria(): void {
    this.$scope.faixaEtariaLoading = false;
    var qtde_faixa_etaria = new Array<RowChart>();
    qtde_faixa_etaria[0] = {
      c: [{v: 'Até 20 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[1] = {
      c: [{v: '21 a 25 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[2] = {
      c: [{v: '26 a 30 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[3] = {
      c: [{v:  '31 a 35 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[4] = {
      c: [{v: '36 a 40 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[5] = {
      c: [{v: '41 a 45 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[6] = {
      c: [{v: '46 a 50 anos'}, {v: 0}]
    };
    qtde_faixa_etaria[7] = {
      c: [{v: 'Maior que 50 anos'}, {v: 0}]
    };

    this.$scope.listDados.forEach(element => {
      qtde_faixa_etaria[element.faixa_etaria-1].c[1].v++;
    });

    this.$scope.faixaEtariaChartObject = {};
    this.$scope.faixaEtariaChartObject.type = "PieChart";
    this.$scope.faixaEtariaChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Slices", type: "number"}
    ], "rows": qtde_faixa_etaria};
  }

  public geraGraficoAprendizagem(): void {
    this.$scope.aprendizagemloading = false;
    var aprendizagem = new Array<RowChart>();

    aprendizagem[0] = {
      c: [{v: 'Não aprendeu nada'}, {v: 0}]
    };
    aprendizagem[1] = {
      c: [{v: 'Relembrou algo'}, {v: 0}]
    };
    aprendizagem[2] = {
      c: [{v: 'Aprendeu algo'}, {v: 0}]
    };
    aprendizagem[3] = {
      c: [{v: 'Reforçou o conhecimento'}, {v: 0}]
    }

    this.$scope.listDados.forEach(element => {
      aprendizagem[element.aprendeu-1].c[1].v++;
    });

    this.$scope.aprendizagemChartObject = {};
    this.$scope.aprendizagemChartObject.type = "PieChart";
    this.$scope.aprendizagemChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Quantidade de pessoas", type: "number"}
    ], "rows": aprendizagem};
  }

  public geraGraficoVersaoVR(): void {
    this.$scope.versaoVRloading = false;
    var versaoVR = new Array<RowChart>();

    versaoVR[0] = {
      c: [{v: 'Sim'}, {v: 0}]
    };
    versaoVR[1] = {
      c: [{v: 'Não'}, {v: 0}]
    };

    this.$scope.listDados.forEach(element => {
      versaoVR[element.jogar_vr-1].c[1].v++;
    });

    this.$scope.versaoVRChartObject = {};
    this.$scope.versaoVRChartObject.type = "PieChart";
    this.$scope.versaoVRChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Quantidade de pessoas", type: "number"}
    ], "rows": versaoVR};
  }

  public geraGraficoPontuacaoXCalculoI(): void {
    this.$scope.calculoIPontuacaoLoading = false;
    var calculoI = new Array<RowChart>();

    calculoI[0] = {
      c: [{v: 'Concluiu'}, {v: 0}]
    };
    calculoI[1] = {
      c: [{v: 'Cursando'}, {v: 0}]
    };
    calculoI[2] = {
      c: [{v: 'Reprovou'}, {v: 0}]
    };
    calculoI[3] = {
      c: [{v: 'Nunca cursou'}, {v: 0}]
    };

    var qtde: number[] = [0, 0, 0, 0];
    this.$scope.listDados.forEach(element => {
      calculoI[element.cursou_calculo-1].c[1].v += parseInt(element.pontos, 10);
      qtde[element.cursou_calculo-1]++;
    });

    var i = 0;
    calculoI.forEach(calculo => {
      calculo.c[1].v = calculo.c[1].v / qtde[i++];
    });


    this.$scope.calculoIPontuacaoChartObject = {};
    this.$scope.calculoIPontuacaoChartObject.type = "BarChart";
    this.$scope.calculoIPontuacaoChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Média da Pontuação", type: "number"}
    ], "rows": calculoI};
  }


  public geraGraficoCalculoI(): void {
    this.$scope.calculoILoading = false;
    var calculoI = new Array<RowChart>();

    calculoI[0] = {
      c: [{v: 'Concluiu'}, {v: 0}]
    };
    calculoI[1] = {
      c: [{v: 'Cursando'}, {v: 0}]
    };
    calculoI[2] = {
      c: [{v: 'Reprovou'}, {v: 0}]
    };
    calculoI[3] = {
      c: [{v: 'Nunca cursou'}, {v: 0}]
    };

    var qtde: number[] = [0, 0, 0, 0];
    this.$scope.listDados.forEach(element => {
      calculoI[element.cursou_calculo-1].c[1].v++;
    });


    this.$scope.calculoIChartObject = {};
    this.$scope.calculoIChartObject.type = "PieChart";
    this.$scope.calculoIChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Quantidade alunos", type: "number"}
    ], "rows": calculoI};
  }

  public geraGraficoNivelEnsino(): void {
    this.$scope.nivelEnsinoLoading = false;
    var qtde_nivel_ensino = new Array<RowChart>();
    qtde_nivel_ensino[0] = {
      c: [{v: 'Médio cursando'}, {v: 0}]
    };
    qtde_nivel_ensino[1] = {
      c: [{v: 'Médio completo'}, {v: 0}]
    };
    qtde_nivel_ensino[2] = {
      c: [{v: 'Superior cursando'}, {v: 0}]
    };
    qtde_nivel_ensino[3] = {
      c: [{v:  'Superior completo'}, {v: 0}]
    };
    qtde_nivel_ensino[4] = {
      c: [{v: 'Especialização cursando'}, {v: 0}]
    };
    qtde_nivel_ensino[5] = {
      c: [{v: 'Especialização completo'}, {v: 0}]
    };

    this.$scope.listDados.forEach(element => {
      qtde_nivel_ensino[element.nivel_ensino-1].c[1].v++;
    });

    this.$scope.nivelEnsinoChartObject = {};
    this.$scope.nivelEnsinoChartObject.type = "PieChart";
    this.$scope.nivelEnsinoChartObject.data = {"cols": [
        {id: "t", label: "Topping", type: "string"},
        {id: "s", label: "Slices", type: "number"}
    ], "rows": qtde_nivel_ensino};
  }

  public listDados(): void {
    this.$http.get(this.URL_BACKEND).success((retorno: any) => {
        this.$scope.listDados = retorno;
        this.geraGraficoFaixaEtaria();
        this.geraGraficoAprendizagem();
        this.geraGraficoPontuacaoXCalculoI();
        this.geraGraficoNivelEnsino();
        this.geraGraficoVersaoVR();
        this.geraGraficoCalculoI();
      }).error((erro: any) => {
        this.$scope.erroBackend = true;
    });
  }
}
