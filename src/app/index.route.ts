/** @ngInject */
export function routerConfig($routeProvider: angular.route.IRouteProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'app/jogo/jogo.html',
      controller: 'JogoController',
      controllerAs: 'controller'
    })
    .when('/graficos', {
      templateUrl: 'app/graficos/graficos.html',
      controller: 'GraficosController',
      controllerAs: 'controller'
    })
    .otherwise({
      redirectTo: '/'
    });
}
