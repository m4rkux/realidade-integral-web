export class ModelForm {
    pontos: number;
    faixa_etaria: number;
    nivel_ensino: number;
    cursou_calculo: number;
    aprendeu: number;
    jogar_vr: number;
    feedback: string;
}
