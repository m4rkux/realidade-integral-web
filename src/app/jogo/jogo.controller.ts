import { ModelForm } from './ModelForm';
import { Fase, Dica } from './ModelFase';

interface IJogo extends ng.IScope {
    fases: Fase[];
    fase: Fase;
    pontos: number;
    dicaRevelada: boolean;
    zerou: boolean;
    form: ModelForm;
    erroBackend: boolean;
    feedbackEnviado: boolean;
    posicaoOpcaoFase: number;
}

export class JogoController {

  public static $inject: string[] = new Array<string>('$scope', 'toastr', '$http');
  public static ID: string = 'JogoController';
  public $scope: IJogo;
  private toastr: any;
  private $http: ng.IHttpService;
  private URL_BACKEND = 'http://realidadeintegral.tambacode.com.br/api/public/salva-dados';
  // private URL_BACKEND = 'http://localhost:8000/';

  /* @ngInject */
  constructor ($scope: IJogo, toastr: any, $http: ng.IHttpService) {
    this.$scope = $scope;
    this.toastr = toastr;
    this.$http = $http;

    this.init();
  }

  public init(): void {
    this.$scope.erroBackend = false;
    this.$scope.feedbackEnviado = false;
    this.$scope.zerou = false;
    this.$scope.dicaRevelada = false;
    this.$scope.pontos = 0;
    this.$scope.fase = new Fase(); 
    this.$scope.fase.dica = new Dica();

    this.$scope.form = new ModelForm();
    this.initFases();
  }

  public initFases(): void {

     this.$http.get('app/jogo/fases.json')
          .success((retorno: any) => {
              this.$scope.fases = retorno;
              this.$scope.fase = this.$scope.fases[0];
              this.$scope.posicaoOpcaoFase = Math.floor((Math.random() * this.$scope.fase.opcoes_fase.length));
          }).error((erro: any) => {
            
        });
  }


  public clickResposta(indice: number): void {
    if(this.$scope.fase.opcoes_fase[this.$scope.posicaoOpcaoFase].alternativas[indice].disponivel) {
      this.$scope.fase.opcoes_fase[this.$scope.posicaoOpcaoFase].alternativas[indice].disponivel = false;

      if(this.$scope.fase.opcoes_fase[this.$scope.posicaoOpcaoFase].alternativas[indice].certa) {
        this.toastr.success('+' + this.$scope.fase.acerto + ' pontos.', 'Resposta certa');
        this.$scope.pontos += this.$scope.fase.acerto;

        if(this.$scope.fase.numero < this.$scope.fases.length) {
          this.$scope.dicaRevelada = false;
          this.$scope.fase = this.$scope.fases[this.$scope.fase.numero];
          this.$scope.posicaoOpcaoFase = Math.floor((Math.random() * this.$scope.fase.opcoes_fase.length));
        } else {
          this.$scope.zerou = true;
          this.$scope.form.pontos = this.$scope.pontos;
        }
        

      } else {
        this.$scope.pontos -= this.$scope.fase.erro;
        this.toastr.error('-' + this.$scope.fase.erro + ' pontos.', 'Resposta errada');
      }
    }
    
  }

  public revelarDica(): void {
    if (!this.$scope.dicaRevelada) {
      this.$scope.pontos -= this.$scope.fase.dica.pontos;
      this.toastr.info('-' + this.$scope.fase.dica.pontos + ' pontos.', 'Uso da dica');
      this.$scope.dicaRevelada = true;
    }
  }

  public enviaDados(): void {
    this.$http.post(this.URL_BACKEND, this.$scope.form).success((retorno: any) => {
            this.$scope.feedbackEnviado = true;
          }).error((erro: any) => {
            this.$scope.erroBackend = true;
        });
  }
}
