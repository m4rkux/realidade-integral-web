export class Dica {
    pontos: number;
    texto: string;
}

class Alternativa {
    img: string;
    label: string;
    certa: boolean;
    disponivel: boolean;
}

class OpcaoFase {
    pergunta: string;
    alternativas: Alternativa[];
}

export class Fase {
    numero: number;
    label: string;
    erro: number;
    acerto: number;
    opcoes_fase: OpcaoFase[];
    dica: Dica = new Dica();
}

